import { HomeComponent } from './views/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: '',
                redirectTo: 'teams',
                pathMatch: 'full'
            },
            {
                path: 'teams',
                loadChildren: () => import("./views/team/team.module").then(m => m.TeamModule)
            },
            {
                path: 'players/:id',
                loadChildren: () => import("./views/player/player.module").then(m => m.PlayerModule)
            }
        ]

    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
