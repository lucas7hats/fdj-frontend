import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, take, catchError } from 'rxjs';
import {throwError as observableThrowError} from 'rxjs/internal/observable/throwError';
import { environment } from 'src/environments/environment';

const BACKEND_URL = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class DatabaseSyncService {

  constructor(private http: HttpClient) { }

  initLeague() {
    return this.http.get<any>(`${BACKEND_URL}${'leagues/init'}`).pipe(
      map((data: any) => data), take(1), catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    
    );
  }


}
