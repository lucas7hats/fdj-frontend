import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, take } from 'rxjs';
import {throwError as observableThrowError} from 'rxjs/internal/observable/throwError';
import { environment } from 'src/environments/environment';
const BACKEND_URL = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http: HttpClient) { }

  getPlayers(id: string) {
    return this.http.get<any>(`${BACKEND_URL}${'players/'}${id}`).pipe(
      map((data: any) => data), take(1), catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    
    );
  }
}
