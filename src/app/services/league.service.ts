import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, take } from 'rxjs';
import {throwError as observableThrowError} from 'rxjs/internal/observable/throwError';
import { environment } from 'src/environments/environment';

const BACKEND_URL = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class LeagueService {


  constructor(private http: HttpClient) { }

  getLeagues(word: string) {
    return this.http.post<any>(`${BACKEND_URL}${'leagues/list'}`, {"search": word}).pipe(
      map((data: any) => data), take(1), catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    
    );
  }
}
