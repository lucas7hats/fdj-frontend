export interface Team {
    idTeam: string;
    strTeam: string;
    strTeamShort: string;
    strAlternate:string;
    intFormedYear: string;
    strLeague: string;
    idLeague: string;
    strRSS: string;
    strStadiumThumb: string;
    strStadiumDescription: string;
    strStadiumLocation:string;
    intStadiumCapacity: string;
    strWebsite: string;
    strFacebook: string;
    strTwitter: string;
    strInstagram: string;
    strDescriptionEN: string;
    strCountry: string;
    strTeamBadge: string;
    strTeamJersey: string;
    strTeamLogo: string;
    strTeamFanart1: string;
    strTeamFanart2: string;
    strTeamFanart3: string;
    strTeamFanart4: string;
    strTeamBanner: string;
    strYoutube: string;
}