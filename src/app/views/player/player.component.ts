import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { PlayerService } from 'src/app/services/player.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
  isLoading: boolean = false;
  private _destroy$ = new Subject();
  players: any = [];
  teamId: string = '';
  teamData: any;

  constructor(private router:Router, private playerService: PlayerService, private activatedroute: ActivatedRoute) { 
    this.teamId = this.activatedroute.snapshot.params['id'];
    this.teamData = this.router.getCurrentNavigation()?.extras.state;
  }


  getPlayers(id: string) {
    this.isLoading = true;
      this.playerService.getPlayers(id).pipe(takeUntil(this._destroy$)).subscribe(res => {
        if(res) {
          this.players = res.players;
          this.isLoading = false;
        }
      });
  }

  ngOnInit(): void {
    this.getPlayers(this.teamId);
    
  }

}
