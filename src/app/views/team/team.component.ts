import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil , finalize} from 'rxjs';
import { LeagueService } from 'src/app/services/league.service';
import { TeamService } from 'src/app/services/team.service';


@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit, OnDestroy {
  private _destroy$ = new Subject();
  isItem = false;
  items: any = [];
  barSize = 12;
  showCancel= false;
  teams: any = [];
  showTeams = true;

  constructor(private leagueService: LeagueService, private teamService: TeamService) { }

  loadLeague(word: string) {
    this.leagueService.getLeagues(word).pipe(takeUntil(this._destroy$)).subscribe(res => {
      if(res) {
        this.items = res.leagues;
        if(this.items.length > 0) this.showTeams = false;
      }
    });
  }

  checkBlur() {
    this.barSize = 12;
    this.showCancel = false;
  }

  checkFocus() {
    this.barSize = 10;
    this.showCancel = true;
  }

  cancel() {
    this.items = [];
  }

  getTeams(id: string) {
    this.teamService.getTeams(id).pipe(takeUntil(this._destroy$)).subscribe(res => {
      if(res) {
        this.teams = res.teams;
        this.showTeams = true;
        this.items = [];
      }
    });
  }

  getItems(e: any) {
    const val = e.target.value;
    this.loadLeague(val.toString().toLowerCase());

    if(val && val.trim() !== '') {
      this.isItem = true;
      this.items = this.items?.filter((item: any) => {
        return (item.strLeague.toString().toLowerCase().indexOf(val.toString().toLowerCase()) > -1);
      })
    }
  }

  ngOnInit(): void {
   
  }

  ngOnDestroy(): void {
    this._destroy$.next(null);
    this._destroy$.complete();
  }

}
