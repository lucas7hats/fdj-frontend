import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './spinner/spinner.component';
import { SkeletonComponent } from './skeleton/skeleton.component';
import { AlertComponent } from './alert/alert.component';



@NgModule({
  declarations: [
    SpinnerComponent,
    SkeletonComponent,
    AlertComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SpinnerComponent,
    SkeletonComponent,
    AlertComponent
  ],
  schemas:      [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
})
export class SharedModule { }
